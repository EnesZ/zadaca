package com.example.hp.myapplication;

import android.animation.Animator;
import android.animation.FloatArrayEvaluator;
import android.content.Context;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    RelativeLayout relativeLayout;
    MediaPlayer media;
    SeekBar volumeControl;
    SeekBar progressControl;
    Button b1,b2,b3,b4;
    TableRow row1,row2;
    TableLayout table;

    boolean flag=true;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        relativeLayout = (RelativeLayout)findViewById(R.id.relative_view);
        media = MediaPlayer.create(this,R.raw.feels);//default
        volumeControl = (SeekBar)findViewById(R.id.volume_seek);
        progressControl = (SeekBar)findViewById(R.id.progress_seek);
        b1 = (Button)findViewById(R.id.b1);
        b2 = (Button)findViewById(R.id.b2);
        b3 = (Button)findViewById(R.id.b3);
        b4 = (Button)findViewById(R.id.b4);
        row1 = (TableRow) findViewById(R.id.row1);
        row2 = (TableRow) findViewById(R.id.row2);
        table = (TableLayout) findViewById(R.id.table_layout);

        row1.setClipChildren(false);
        row2.setClipChildren(false);
        row1.setClipToPadding(false);
        row2.setClipToPadding(false);
        table.setClipChildren(false);
        table.setClipToPadding(false);

        relativeLayout.setTranslationY(500);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag){
                    media=MediaPlayer.create(getApplicationContext(),R.raw.feels);
                    media.start();

                    relativeLayout.animate().translationY(0).setDuration(2500);

                    b2.animate().alpha(0).setDuration(1000);
                    b3.animate().alpha(0).setDuration(1000);
                    b4.animate().alpha(0).setDuration(1000);

                    b2.setEnabled(false);
                    b3.setEnabled(false);
                    b4.setEnabled(false);

                    b1.animate().scaleX(1.5f).setDuration(2000);
                    b1.animate().scaleY(1.5f).setDuration(2000);
                    b1.animate().translationX(180).translationY(180).setDuration(5500);


                    flag=false;

                }else {

                    media.pause();

                    relativeLayout.animate().translationY(500).setDuration(2500);

                    b1.animate().scaleX(1).setDuration(2000);
                    b1.animate().scaleY(1).setDuration(2000);
                    b1.animate().translationY(0).setDuration(2500);
                    b1.animate().translationX(0).setDuration(2500);

                    b2.animate().alpha(1).setDuration(1000);
                    b3.animate().alpha(1).setDuration(1000);
                    b4.animate().alpha(1).setDuration(1000);

                    b2.setEnabled(true);
                    b3.setEnabled(true);
                    b4.setEnabled(true);

                    flag=true;
                }
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(flag){
                        media=MediaPlayer.create(getApplicationContext(),R.raw.clint_eastwood);
                        media.start();


                        relativeLayout.animate().translationY(0).setDuration(2500);

                        b1.animate().alpha(0).setDuration(1000);
                        b3.animate().alpha(0).setDuration(1000);
                        b4.animate().alpha(0).setDuration(1000);

                        b1.setEnabled(false);
                        b3.setEnabled(false);
                        b4.setEnabled(false);

                        b2.animate().scaleX(1.5f).setDuration(2000);
                        b2.animate().scaleY(1.5f).setDuration(2000);
                        b2.animate().translationY(180).translationX(-180).setDuration(5500);

                        flag=false;
                    }else {

                        media.pause();

                        relativeLayout.animate().translationY(500).setDuration(2500);

                        b2.animate().scaleX(1).scaleY(1).setDuration(2000);
                        b2.animate().scaleY(1).setDuration(2000);
                        b2.animate().translationY(0).translationX(0).setDuration(2500);


                        b1.animate().alpha(1).setDuration(1000);
                        b3.animate().alpha(1).setDuration(1000);
                        b4.animate().alpha(1).setDuration(1000);
                        b1.setEnabled(true);
                        b3.setEnabled(true);
                        b4.setEnabled(true);

                        flag=true;

                    }

            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag){
                    media=MediaPlayer.create(getApplicationContext(),R.raw.virus);
                    media.start();

                    relativeLayout.animate().translationY(0).setDuration(2500);

                    b2.animate().alpha(0).setDuration(1000);
                    b1.animate().alpha(0).setDuration(1000);
                    b4.animate().alpha(0).setDuration(1000);

                    b2.setEnabled(false);
                    b1.setEnabled(false);
                    b4.setEnabled(false);

                    b3.animate().scaleX(1.5f).setDuration(2000);
                    b3.animate().scaleY(1.5f).setDuration(2000);
                    b3.animate().translationX(180).translationY(-180).setDuration(5500);

                    flag=false;

                }else {

                    media.pause();

                    relativeLayout.animate().translationY(500).setDuration(2500);

                    b3.animate().scaleX(1).setDuration(2000);
                    b3.animate().scaleY(1).setDuration(2000);
                    b3.animate().translationY(0).setDuration(2500);
                    b3.animate().translationX(0).setDuration(2500);

                    b2.animate().alpha(1).setDuration(1000);
                    b1.animate().alpha(1).setDuration(1000);
                    b4.animate().alpha(1).setDuration(1000);

                    b2.setEnabled(true);
                    b1.setEnabled(true);
                    b4.setEnabled(true);

                    flag=true;
                }
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag){
                    media=MediaPlayer.create(getApplicationContext(),R.raw.iron);
                    media.start();

                    relativeLayout.animate().translationY(0).setDuration(2500);

                    b2.animate().alpha(0).setDuration(1000);
                    b3.animate().alpha(0).setDuration(1000);
                    b1.animate().alpha(0).setDuration(1000);

                    b2.setEnabled(false);
                    b3.setEnabled(false);
                    b1.setEnabled(false);

                    b4.animate().scaleX(1.5f).setDuration(2000);
                    b4.animate().scaleY(1.5f).setDuration(2000);
                    b4.animate().translationX(-180).translationY(-180).setDuration(5500);

                    flag=false;

                }else {

                    media.pause();

                    relativeLayout.animate().translationY(500).setDuration(2500);

                    b4.animate().scaleX(1).setDuration(2000);
                    b4.animate().scaleY(1).setDuration(2000);
                    b4.animate().translationY(0).setDuration(2500);
                    b4.animate().translationX(0).setDuration(2500);

                    b2.animate().alpha(1).setDuration(1000);
                    b3.animate().alpha(1).setDuration(1000);
                    b1.animate().alpha(1).setDuration(1000);

                    b2.setEnabled(true);
                    b3.setEnabled(true);
                    b1.setEnabled(true);

                    flag=true;
                }
            }
        });


        final AudioManager audioMenager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int max = audioMenager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int curr = audioMenager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeControl.setMax(max);
        volumeControl.setProgress(curr);

        volumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioMenager.setStreamVolume(AudioManager.STREAM_MUSIC,progress,0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        progressControl.setMax(media.getDuration());

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                progressControl.setProgress(media.getCurrentPosition());
            }
        },0,500);

        progressControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                media.seekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }








    public void clickB2(View view) {


    }

    public void clickB3(View view) {
    }

    public void clickB4(View view) {
    }

    public void clickB1(View view) {
    }
}
